FROM golang:1.16-alpine

RUN apk update

COPY ./go.mod /app/go.mod

WORKDIR /app
RUN go mod download
COPY . /app

RUN go build -o /vuln-reporter

EXPOSE 8082
CMD [ "/vuln-reporter" ]

